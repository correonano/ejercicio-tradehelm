package tradehelm.com.tradehelmtest.api;

import com.annimon.stream.Collectors;
import com.google.gson.Gson;
import com.google.inject.Inject;

import java.util.Arrays;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import roboguice.RoboGuice;
import rx.Observable;
import tradehelm.com.tradehelmtest.model.ImageItem;
import tradehelm.com.tradehelmtest.TradeHelmTest;

import com.annimon.stream.Stream;

/**
 * Created by Ignacio Saslavsky on 21/9/16.
 * correonano@gmail.com
 */
public class ImgurApi {

    private Api mApi;

    private static final String AUTHORIZATION = "Client-ID 1588a32330b9360";

    @Inject
    public ImgurApi() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.imgur.com/3/gallery/r/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        mApi = retrofit.create(Api.class);
    }

    public static ImgurApi getInstance() {
        return RoboGuice.getInjector(TradeHelmTest.getInstance()).getInstance(ImgurApi.class);
    }

    public Observable<List<ImageItem>> getImageItems() {
        return mApi.getImageItems(AUTHORIZATION)
                .map(response -> {
                    List<ImageItem> list = Arrays.asList(new Gson().fromJson(response.get("data"), ImageItem[].class));

                    return Stream
                        .of(list)
                        .filter(value -> !value.nsfw && "image/jpeg".equals(value.type))
                        .collect(Collectors.toList());
                });
    }
}
