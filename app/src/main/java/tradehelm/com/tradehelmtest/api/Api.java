package tradehelm.com.tradehelmtest.api;

import com.google.gson.JsonObject;

import retrofit2.http.GET;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by Ignacio Saslavsky on 21/9/16.
 * correonano@gmail.com
 */
public interface Api {

    @GET("funny")
    Observable<JsonObject> getImageItems(@Header("Authorization") String authorization);
}
