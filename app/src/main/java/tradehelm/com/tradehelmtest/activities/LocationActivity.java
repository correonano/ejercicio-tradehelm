package tradehelm.com.tradehelmtest.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import tradehelm.com.tradehelmtest.fragments.LocationFragment;
import tradehelm.com.tradehelmtest.util.PermissionHelper;
import tradehelm.com.tradehelmtest.R;

public class LocationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });

        start(LocationFragment.newInstance(), LocationFragment.TAG, false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PermissionHelper.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ((LocationFragment)getSupportFragmentManager().findFragmentByTag(LocationFragment.TAG)).getPosition();
                } else {
                    Toast.makeText(this, "No se obtuvieron los permisos", Toast.LENGTH_LONG);
                }
            }
        }
    }
}

