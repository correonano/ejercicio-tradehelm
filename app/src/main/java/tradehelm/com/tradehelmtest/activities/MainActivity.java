package tradehelm.com.tradehelmtest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.inject.Inject;

import tradehelm.com.tradehelmtest.R;
import tradehelm.com.tradehelmtest.activities.BaseActivity;
import tradehelm.com.tradehelmtest.activities.LocationActivity;
import tradehelm.com.tradehelmtest.api.ImgurApi;
import tradehelm.com.tradehelmtest.fragments.ImagesListFragment;

public class MainActivity extends BaseActivity {

    @Inject
    ImgurApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!isStarted(ImagesListFragment.TAG)) {
            start(ImagesListFragment.newInstance(), ImagesListFragment.TAG, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.get_location:
                Intent intent = new Intent(this, LocationActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
