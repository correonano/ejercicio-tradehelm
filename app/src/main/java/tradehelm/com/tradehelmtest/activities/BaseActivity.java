package tradehelm.com.tradehelmtest.activities;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import roboguice.activity.RoboActionBarActivity;
import tradehelm.com.tradehelmtest.R;

/**
 * Created by Ignacio Saslavsky on 15/9/16.
 * correonano@gmail.com
 */
public class BaseActivity extends RoboActionBarActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void start(Fragment fragment, String tag, boolean addToBackStack) {
        start(fragment, tag, addToBackStack, R.id.main_content);
    }

    public void start(Fragment fragment,String tag, boolean addToBackStack, int containerViewId) {
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction().replace(containerViewId, fragment, tag);
        if (addToBackStack) {
            tx.addToBackStack(fragment.getClass().getSimpleName());
        }
        tx.commit();
    }

    public boolean isStarted(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag) != null;
    }

    public boolean popBackStack() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            return true;
        }
        return false;
    }

    public void stopLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showLoading(boolean cancelable) {
        showLoading(cancelable, R.string.loading);
    }

    public void showLoading(boolean cancelable, int message) {
        showLoading(cancelable, getString(message));
    }

    public void showLoading(boolean cancelable, String message) {
        if (progressDialog == null) {
            runOnUiThread(() -> {
                progressDialog = ProgressDialog.show(this, null, message, true, cancelable);
                progressDialog.setOnCancelListener(dialog -> progressDialog = null);
                progressDialog.setOnDismissListener(dialog -> progressDialog = null);
            });
        } else {
            Log.d("baseActivity", "Already showing progress dialog.");
        }
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

}

