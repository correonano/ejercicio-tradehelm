package tradehelm.com.tradehelmtest.util;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import tradehelm.com.tradehelmtest.TradeHelmTest;

public class LocationClient implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final Long REQUEST_TIMEOUT = TimeUnit.SECONDS.toMillis(10);
    private static LocationClient sInstance;

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Subscriber<? super Location> mSubscriber;
    private Handler mHandler = new Handler();

    private LocationClient(Subscriber<? super Location> subscriber) {
        mSubscriber = subscriber;
    }

    private void start() {
        mGoogleApiClient = new GoogleApiClient.Builder(TradeHelmTest.getInstance())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void stop() {
        mHandler.removeCallbacksAndMessages(null);
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ContextCompat.checkSelfPermission(TradeHelmTest.getInstance(), android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        LocationRequest request = LocationRequest.create();
        request.setNumUpdates(1);
        request.setExpirationDuration(REQUEST_TIMEOUT);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(request);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> pendingResult = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        pendingResult.setResultCallback(result -> {
            final Status status = result.getStatus();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, request, this);

                    mHandler.postDelayed(() -> {
                        stop();
                        if (mLastLocation != null) {
                            mSubscriber.onNext(mLastLocation);
                            mSubscriber.onCompleted();
                        } else {
                            Toast.makeText(TradeHelmTest.getInstance(), "No se pudo encontrar la posición", Toast.LENGTH_SHORT).show();
                        }
                    }, REQUEST_TIMEOUT);
                    break;
            }
        });

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w("Location", "Location suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        stop();
    }

    public static Observable<Location> requestLocation() {
        return Observable.create(subscriber -> {
            LocationClient client = new LocationClient(subscriber);
            client.start();
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        stop();
        mSubscriber.onNext(location);
        mSubscriber.onCompleted();
    }
}