package tradehelm.com.tradehelmtest.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ignacio Saslavsky on 23/9/16.
 * correonano@gmail.com
 */
public class PermissionHelper {

    Activity activity;

    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    public PermissionHelper(Activity activity) {
        this.activity = activity;
    }

    public boolean hasPermissions() {
        if (isPermission(Manifest.permission.ACCESS_FINE_LOCATION))
            return false;
        if (isPermission(Manifest.permission.ACCESS_COARSE_LOCATION))
            return false;

        return true;
    }

    public void askForPermissions() {
        final List<String> permissionsList = new ArrayList();
        if (isPermission(Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if (isPermission(Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        if (permissionsList.size() > 0) {
            ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
        }
    }

    private boolean isPermission(String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

}