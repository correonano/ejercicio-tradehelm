package tradehelm.com.tradehelmtest.model;

/**
 * Created by Ignacio Saslavsky on 21/9/16.
 * correonano@gmail.com
 */
public class ImageItem {

    public String id;
    public String title;
    public Long views;
    public String link;
    public Long score;
    public boolean nsfw;
    public String type;

}
