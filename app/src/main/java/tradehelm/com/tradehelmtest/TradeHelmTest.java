package tradehelm.com.tradehelmtest;

import android.app.Application;

import roboguice.RoboGuice;

/**
 * Created by Ignacio Saslavsky on 21/9/16.
 * correonano@gmail.com
 */
public class TradeHelmTest extends Application {

    private static TradeHelmTest sInstance;

    public static TradeHelmTest getInstance() {
        return sInstance;
    }

    private static final String PREF = "pref";

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        RoboGuice.setUseAnnotationDatabases(false);
        RoboGuice.injectMembers(this, this);

    }
}
