package tradehelm.com.tradehelmtest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.squareup.picasso.Picasso;

import java.util.List;

import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tradehelm.com.tradehelmtest.model.ImageItem;
import tradehelm.com.tradehelmtest.R;
import tradehelm.com.tradehelmtest.TradeHelmTest;
import tradehelm.com.tradehelmtest.api.ImgurApi;

/**
 * Created by Ignacio Saslavsky on 21/9/16.
 * correonano@gmail.com
 */
@ContentView(R.layout.image_list_fragment)
public class ImagesListFragment extends BaseFragment {

    public static final String TAG = "imageListFragment";

    @Inject
    ImgurApi mApi;

    @InjectView(R.id.image_list)
    private RecyclerView recyclerView;

    private ImageListAdapter adapter;

    private Subscription subscription;

    public static ImagesListFragment newInstance() {
        ImagesListFragment instance = new ImagesListFragment();
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fetchImageItems();

        Log.d("viewcreated", "viewcreated");

    }

    private void fetchImageItems() {
        if(subscription == null) {
            showLoading();
            subscription = mApi.getImageItems()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(imageItems -> {
                        recyclerView.setHasFixedSize(true);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        recyclerView.setLayoutManager(layoutManager);
                        adapter = new ImageListAdapter(imageItems);
                        recyclerView.setAdapter(adapter);
                        stopLoading();
                    }, error -> {
                        Log.d("error", "error");
                        error.printStackTrace();
                    });
        } else {
            if (adapter != null) {
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(adapter);
            } else {
                showLoading();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopLoading();
    }

    public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {

        private List<ImageItem> dataset;

        public class ViewHolder extends RecyclerView.ViewHolder {

            public final TextView title;
            public final ImageView image;
            public final TextView votes;
            public final TextView views;

            public ViewHolder(View view) {
                super(view);
                title = (TextView) view.findViewById(R.id.title_text);
                image = (ImageView) view.findViewById(R.id.imgur_image);
                votes = (TextView) view.findViewById(R.id.votes_text);
                views = (TextView) view.findViewById(R.id.views_text);

            }

        }

        public ImageListAdapter(List<ImageItem> dataset) {
            this.dataset = dataset;
        }

        @Override
        public ImageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_line, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final ImageItem imageItem = dataset.get(position);
            holder.title.setText(imageItem.title);
            holder.votes.setText(imageItem.score.toString());
            holder.views.setText(imageItem.views.toString());
            Picasso.with(TradeHelmTest.getInstance())
                    .load(imageItem.link)
                    .placeholder(R.drawable.imgur)
                    .fit().centerCrop()
                    .into(holder.image);

        }

        @Override
        public int getItemCount() {
            return dataset.size();
        }
    }
}
