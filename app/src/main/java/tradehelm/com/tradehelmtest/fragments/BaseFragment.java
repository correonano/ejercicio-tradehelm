package tradehelm.com.tradehelmtest.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import roboguice.fragment.RoboFragment;
import roboguice.inject.ContentView;
import tradehelm.com.tradehelmtest.activities.BaseActivity;

public class BaseFragment extends RoboFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View superReturn = super.onCreateView(inflater, container, savedInstanceState);

        ContentView contentViewAnn = getClass().getAnnotation(ContentView.class);
        if (contentViewAnn != null) {
            return inflater.inflate(contentViewAnn.value(), container, false);
        }

        return superReturn;
    }

    public boolean finish() {
        if (!getBaseActivity().popBackStack()) {
            getActivity().finish();
        }
        return true;
    }

    protected void stopLoading() {
        if (getBaseActivity() != null) getBaseActivity().stopLoading();
    }

    protected void showLoading() {
        showLoading(false);
    }

    protected boolean isLoading() {
        if (getBaseActivity() != null)
            return getBaseActivity().getProgressDialog() != null;
        return false;
    }

    protected void showLoading(boolean cancelable) {
        if (getBaseActivity() != null) getBaseActivity().showLoading(cancelable);
    }

    protected void showLoading(boolean cancelable, int resId) {
        if (getBaseActivity() != null) getBaseActivity().showLoading(cancelable, resId);
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

}
