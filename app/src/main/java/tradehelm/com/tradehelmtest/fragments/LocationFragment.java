package tradehelm.com.tradehelmtest.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import rx.android.schedulers.AndroidSchedulers;
import tradehelm.com.tradehelmtest.util.LocationClient;
import tradehelm.com.tradehelmtest.util.PermissionHelper;
import tradehelm.com.tradehelmtest.activities.BaseActivity;

/**
 * Created by Ignacio Saslavsky on 23/9/16.
 * correonano@gmail.com
 */
public class LocationFragment extends SupportMapFragment implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback {

    public static final String TAG = "LocationFragment";

    public static final LocationFragment newInstance() {
        LocationFragment instance = new LocationFragment();
        return instance;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    private void center(LatLng latLng) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        getMapAsync(googleMap -> {
            googleMap.animateCamera(cameraUpdate);
            googleMap.addMarker(new MarkerOptions().position(latLng));});
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PermissionHelper helper = new PermissionHelper(getActivity());
        if(helper.hasPermissions()) {
            getPosition();
        } else {
            helper.askForPermissions();
        }
    }

    public void getPosition() {
        ((BaseActivity)getActivity()).showLoading(true, "Obteniendo su posición actual.");
        LocationClient.requestLocation()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(location -> {
                ((BaseActivity)getActivity()).stopLoading();
                center(new LatLng(location.getLatitude(), location.getLongitude()));
                Log.d("location", location.getLatitude() + " " + location.getLongitude());
            }, error -> {
                ((BaseActivity)getActivity()).stopLoading();
            });
    }

}
